#!/bin/sh

TOPIC=test/topic
MESSAGE=helloWorld

# Start Mosquitto Container
docker run \
  --rm \
  -d \
  -p 1883:1883 \
  --name $CIRCLE_PROJECT_REPONAME \
  $CIRCLE_PROJECT_REPONAME

# Run Alpine with link to mosquitto, install mosquitto-clients and subscribe to a message
docker run \
  -ti \
  --link mosquitto:mosquitto \
  --name mosquitto-client \
  -d \
  alpine:3.7 \
  /bin/sh -c \
    "apk add --no-cache mosquitto-clients ; \
    mosquitto_sub -h mosquitto -C 1 -t '$TOPIC'"

# Use mosquitto-clients container to publish a message, container stops as subscriber terminates after receiving 1 message
docker exec -ti mosquitto-client \
  /bin/sh -c \
    "echo Waiting for mosquitto_pub for being available; while [ ! -f /usr/bin/mosquitto_pub ]; do sleep 1; done; \
    mosquitto_pub -h mosquitto -t '$TOPIC' -m '$MESSAGE'; echo 'message sent'"

echo checking docker logs
RECEIVED_MESSAGE=$(docker logs mosquitto-client | tail -1 | sed 's/\r$//')
echo Received message: ${RECEIVED_MESSAGE}
echo Sent message: ${MESSAGE}

if [ "$RECEIVED_MESSAGE" = "$MESSAGE" ]; then
    echo "received message match sent message"
else
    echo "received message ($RECEIVED_MESSAGE) does not match sent message ($MESSAGE)"
    exit 1
fi;
