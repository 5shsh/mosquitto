#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE_NAMESPACE=library
ARG BASE_IMAGE_NAME=alpine
ARG BASE_IMAGE_VERSION=3.8

FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION} as builder

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY ./bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------

RUN apk --no-cache add mosquitto
RUN mkdir -p /mqtt/config

COPY mosquitto.conf /mqtt/config/mosquitto.conf
COPY websockets.conf /mqtt/config/conf.d/websockets.conf

EXPOSE 1883 9001

CMD ["/usr/sbin/mosquitto",  "-c", "/mqtt/config/mosquitto.conf"]

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY ./bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
LABEL de.5square.homesmarthome.build-date=$BUILD_DATE \
      de.5square.homesmarthome.name="homesmarthome/mosquitto" \
      de.5square.homesmarthome.description="MQTT Broker" \
      de.5square.homesmarthome.url="homesmarthome.5square.de" \
      de.5square.homesmarthome.vcs-ref=$VCS_REF \
      de.5square.homesmarthome.vcs-url="$VCS_URL" \
      de.5square.homesmarthome.vendor="5square" \
      de.5square.homesmarthome.version=$VERSION \
      de.5square.homesmarthome.schema-version="1.0"
