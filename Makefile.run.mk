docker_run:
	docker run -d \
	  --rm \
	  --name=mosquitto_test_run \
	  -p 1883:1883 \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep mosquitto_test_run

docker_stop:
	docker rm -f mosquitto_test_run 2> /dev/null ; true